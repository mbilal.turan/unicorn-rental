const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('..');
const db = require('../src/lib/db');

chai.use(chaiHttp);

const requester = chai.request;
const { expect } = chai;

const ONE_SECOND = 1000;
const ONE_MINUTE = 60 * ONE_SECOND;
const ONE_HOUR = 60 * ONE_MINUTE;
const ONE_MIN_LESS_THAN_ONE_HOUR = ONE_HOUR - ONE_MINUTE;
const ONE_SECOND_MORE_THAN_ONE_HOUR = ONE_HOUR + ONE_SECOND;
const TWENTY_HOURS = 20 * ONE_HOUR;
const THREE_HOURS_AND_A_HALF = 3.5 * ONE_HOUR;

// describe('When renting a unicorn', () => {
//   beforeEach(() => {
//     db.resetDB();
//   });

//   it('should let user rent an available unicorn', async () => {
//     const beforeRequest = new Date();
//     const response = await requester(app).post('/rentals')
//       .send({
//         type: 'Unicorn',
//         name: 'Twilight Sparkle',
//       });
//     expect(response).to.have.status(200);
//     expect(response.body.status).to.equal('success');
//     expect(response.body.name).to.equal('Twilight Sparkle');
//     expect(response.body.hourlyRate).to.equal(25);

//     const afterRequest = new Date();
//     expect(new Date(response.body.rentStartDateTime)).to.be.within(beforeRequest, afterRequest);
//   });

//   it('should not let user rent a unicorn that is already rented', async () => {
//     await requester(app).post('/rentals')
//       .send({
//         type: 'Unicorn',
//         name: 'Twilight Sparkle',
//       });
//     const response = await requester(app).post('/rentals')
//       .send({
//         type: 'Unicorn',
//         name: 'Twilight Sparkle',
//       });


//     expect(response).to.have.status(500);
//     expect(response.body.errors.message).to.equal('Unicorn is already rented out');
//   });

//   // it('should not let user rent a unicorn that is resting', async () => {
//   //   await requester(app).post('/rentals')
//   //     .send({
//   //       type: 'Unicorn',
//   //       name: 'Twilight Sparkle',
//   //     });

//   //   await requester(app).patch('/rentals')
//   //     .send({
//   //       type: 'Unicorn',
//   //       name: 'Twilight Sparkle',
//   //     });
//   //   const response = await requester(app).get('/unicorns/rentals/Twilight Sparkle');
//   //   expect(response).to.have.status(400);
//   //   expect(response.body.status).to.equal('failure');
//   //   expect(response.body.error).to.equal('Unicorn is resting and will be available in 30 minutes');
//   // });

//   // it('should not let user rent a non-existent unicorn', async () => {
//   //   const response = await requester(app).get('/unicorns/rentals/Non-existent');
//   //   expect(response).to.have.status(400);
//   //   expect(response.body.status).to.equal('failure');
//   //   expect(response.body.error).to.equal('Unicorn does not exist');
//   // });
// });


describe('When returning a unicorn', () => {
  beforeEach(() => {
    db.resetDB();
  });

  it('should allow returning a rented unicorn', async () => {
    db.update({ name: 'Twilight Sparkle', type: 'Unicorn' }, { rentStartDateTime: Date.now() - ONE_MINUTE });

    const response = await requester(app).patch('/rentals')
      .send({
        type: 'Unicorn',
        name: 'Twilight Sparkle',
      });

    expect(response).to.have.status(200);
    expect(response.body.status).to.equal('success');
    expect(response.body.name).to.equal('Twilight Sparkle');
    expect(response.body.type).to.equal('Unicorn');
    expect(response.body.hourlyRate).to.equal(25);
    expect(response.body.billableHours).to.equal(1);
    expect(response.body.totalOwed).to.equal(25);
  });

  // it('should correctly calculate amount owed', async () => {
  //   db.update('Twilight Sparkle', 'rentStartDateTime', Date.now() - ONE_SECOND_MORE_THAN_ONE_HOUR);
  //   db.update('Rainbow Dash', 'rentStartDateTime', Date.now() - TWENTY_HOURS);
  //   db.update('Pinkie Pie', 'rentStartDateTime', Date.now() - ONE_MIN_LESS_THAN_ONE_HOUR);
  //   db.update('Fluttershy', 'rentStartDateTime', Date.now() - THREE_HOURS_AND_A_HALF);

  //   const twilightSparkleReturned = await requester(app).post('/unicorns/rentals/Twilight Sparkle');
  //   const rainbowDashReturned = await requester(app).post('/unicorns/rentals/Rainbow Dash');
  //   const pinkiePieReturned = await requester(app).post('/unicorns/rentals/Pinkie Pie');
  //   const fluttershyReturned = await requester(app).post('/unicorns/rentals/Fluttershy');


  //   expect(twilightSparkleReturned.body.billableHours).to.equal(2);
  //   expect(twilightSparkleReturned.body.totalOwed).to.equal(16);

  //   expect(rainbowDashReturned.body.billableHours).to.equal(21);
  //   expect(rainbowDashReturned.body.totalOwed).to.equal(168);

  //   expect(pinkiePieReturned.body.billableHours).to.equal(1);
  //   expect(pinkiePieReturned.body.totalOwed).to.equal(8);

  //   expect(fluttershyReturned.body.billableHours).to.equal(4);
  //   expect(fluttershyReturned.body.totalOwed).to.equal(32);
  // });

  // it('should not let user return a unicorn that is not rented', async () => {
  //   const response = await requester(app).post('/unicorns/rentals/Fluttershy');

  //   expect(response).to.have.status(400);
  //   expect(response.body.status).to.equal('failure');
  //   expect(response.body.error).to.equal('You cannot return an unicorn that is not rented out');
  // });

  // it('should not let user return a non-existent unicorn', async () => {
  //   const response = await requester(app).post('/unicorns/rentals/Non-existent');

  //   expect(response).to.have.status(400);
  //   expect(response.body.status).to.equal('failure');
  //   expect(response.body.error).to.equal('Type of animal does not exist');
  // });
});


describe('healthcheck', () => {
  it('should respond', async () => {
    const response = await requester(app).get('/healtz');
    expect(response.text).to.equal('All systems nominal, stablemaster.');
  });
});
