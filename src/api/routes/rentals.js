const { Router } = require('express');

const rentalService = require('../../services/rental-service');

const route = Router();
module.exports = (app) => {
  app.use('/rentals', route);

  route.get('/', (req, res, next) => {
    try {
      const result = rentalService.getAllTaxi();
      return res.json(result);
    } catch (e) {
      console.log('Error:', e);
      return next(e);
    }
  });


  route.post('/', async (req, res, next) => {
    try {
      const { name, type } = req.body;
      const result = await rentalService.rentTaxi({ name, type });
      res.json(result);
    } catch (e) {
      console.log('Error:', e);
      return next(e);
    }
  });


  route.patch('/', async (req, res, next) => {
    try {
      const { name, type } = req.body;
      const result = await rentalService.returnTaxi({ name, type });
      res.json(result);
    } catch (e) {
      console.log('Error:', e);
      return next(e);
    }
  });
};
