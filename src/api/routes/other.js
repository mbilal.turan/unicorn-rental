const { Router } = require('express');

const route = Router();

module.exports = (app) => {
  app.use('/', route);

  route.get('/', (req, res) => {
    res.send('<h1 style="height: 10em;  display: flex; align-items: center;  justify-content: center">Unicorn taxi service open for business.</h1>');
  });

  route.get('/healtz', (req, res) => {
    res.send('All systems nominal, stablemaster.');
  });

  route.get('*', (req, res) => {
    res.status(404).send('There ain\'t nopony here.');
  });

  return route;
};
