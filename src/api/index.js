const { Router } = require('express');

const rentals = require('./routes/rentals');
const other = require('./routes/other');

const app = Router();
rentals(app);
other(app);

module.exports = app;
