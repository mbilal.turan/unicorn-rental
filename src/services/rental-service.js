const db = require('../lib/db');

function getMinutesLeftToDateTime(timeInFuture, now) {
  return Math.ceil((timeInFuture - now) / 1000 / 60);
}

function getTaxiStatus({ rideable }) {
  const now = new Date();

  const response = {
    isRentable: true,
  };

  // if rentStart exists, the beast is already rented, not possible to rent.
  // Similar logic for next rentable date time, the beast is resting.
  if (rideable.rentStartDateTime) {
    response.isRentable = false;
    response.reason = 'Unicorn is already rented out';
  }

  if (now < rideable.nextRentableDateTime) {
    response.isRentable = false;
    response.reason = `Unicorn is resting and will be available in ${getMinutesLeftToDateTime(rideable.nextRentableDateTime, now)} minutes`;
  }

  return response;
}

function calculateBillableHours(rentStartDateTime) {
  const now = new Date();
  const timeSpentRentingUnicorn = now - rentStartDateTime;
  const hoursRented = Math.ceil(timeSpentRentingUnicorn / 60 / 60 / 1000);
  return hoursRented;
}

function calculatePrice({ billableHours, hourlyRate }) {
  return Math.floor(billableHours * hourlyRate);
}

async function sendToRest({ rideable }) {
  const now = Date.now();
  const neededRest = rideable.neededRestTimeInMs;
  await db.update({ name: rideable.name, type: rideable.type }, { nextRentableDateTime: now + neededRest });
  await db.update({ name: rideable.name, type: rideable.type }, { rentStartDateTime: null });
}


async function returnTaxi({ name, type }) {
  const rideable = await db.get({ name, type });

  // If the unicorn is not rented out, it cannot be returned.
  if (!rideable.rentStartDateTime) {
    throw new Error('You cannot return an unicorn that is not rented out');
  }

  const billableHours = calculateBillableHours(rideable.rentStartDateTime);
  const totalOwed = calculatePrice({ billableHours, hourlyRate: rideable.hourlyRate });
  await sendToRest({ rideable });

  return {
    status: 'success',
    billableHours,
    hourlyRate: rideable.hourlyRate,
    name,
    type,
    totalOwed,
  };
}

async function rentTaxi({ name, type }) {
  const now = new Date();
  const rideable = await db.get({ name, type });
  const unicornStatus = getTaxiStatus({ rideable });

  if (!unicornStatus.isRentable) {
    throw new Error(unicornStatus.reason);
  }

  await db.update({ name, type }, { rentStartDateTime: now });

  return {
    status: 'success',
    name,
    rentStartDateTime: now.toISOString(),
    hourlyRate: rideable.hourlyRate,
  };
}

async function getAllTaxi() {
  return db.getAllRideable();
}


module.exports = {
  returnTaxi,
  rentTaxi,
  getAllTaxi,
};
