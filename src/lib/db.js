const Loki = require('lokijs');

class DBInstance {
  constructor() {
    this.initDB();
    this.seedDB();
  }

  async initDB() {
    this.db = new Loki('mythology-taxi.db');
    this.MythologyTaxi = this.db.addCollection('mythology-taxi');
  }

  async seedDB() {
    const mythologyList = [
      {
        name: 'Pinkie Pie',
        type: 'Unicorn',
        rentStartDateTime: null,
        nextRentableDateTime: new Date(),
        neededRestTimeInMs: 15 * 60 * 1000,
        hourlyRate: 8,
      },
      {
        name: 'Rainbow Dash',
        type: 'Unicorn',
        rentStartDateTime: null,
        nextRentableDateTime: new Date(),
        neededRestTimeInMs: 15 * 60 * 1000,
        hourlyRate: 12,
      },
      {
        name: 'Fluttershy',
        type: 'Unicorn',
        rentStartDateTime: null,
        nextRentableDateTime: new Date(),
        neededRestTimeInMs: 15 * 60 * 1000,
        hourlyRate: 14,
      },
      {
        name: 'Twilight Sparkle',
        type: 'Unicorn',
        rentStartDateTime: null,
        nextRentableDateTime: new Date(),
        neededRestTimeInMs: 30 * 60 * 1000,
        hourlyRate: 25,
      },
    ];
    mythologyList.forEach((rideable) => {
      this.MythologyTaxi.insert(rideable);
    });
  }

  async update({ name, type }, toUpdate) {
    const rideable = this.MythologyTaxi.findOne({ name, type });

    if (!rideable) {
      throw new Error('Unicorn does not exist');
    }

    const toUpdateArray = Object.entries(toUpdate);
    toUpdateArray.forEach((keyAndValue) => {
      const key = keyAndValue[0];
      const value = keyAndValue[1];

      rideable[key] = value;
    });

    this.MythologyTaxi.update(rideable);
  }

  async get({ name, type }) {
    const unicorn = await this.MythologyTaxi.findOne({ name, type });
    console.log(name, type);
    if (!unicorn) {
      throw new Error('Unicorn does not exist');
    }

    return unicorn;
  }

  async getAllRideable() {
    return this.MythologyTaxi.find();
  }

  resetDB() {
    this.MythologyTaxi.clear();
    this.seedDB();
  }
}

const db = new DBInstance();

module.exports = db;
