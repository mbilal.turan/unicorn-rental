const expressLoader = require('./express');

module.exports = ({ app }) => {
  expressLoader({ app });
  console.log('✌️ Express loaded');
};
