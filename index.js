const express = require('express');
const loaders = require('./src/loaders');

const PORT = 8081;

const app = express();

try {
  loaders({ app });

} catch (error) {
  console.log(error)
}

app.listen(PORT, () => {
  console.log(`
      ################################################
              Server listening on port: ${PORT}
      ################################################
    `);
});

module.exports = app;
