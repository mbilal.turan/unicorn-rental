# API Template

## Table of Contents
+ [About](#about)
+ [Getting Started](#getting-started)
+ [Usage](#usage)

## About
API template

## Getting Started
These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

- NodeJS 6+

### Installing

```
npm install
```

### Running the application

```
npm start
```

### Running the tests

```
npm test
```

### Running the lint check

```
npm run lint
```

## Usage


add how to use api next time
